package MyRequest

import (
	"github.com/levigross/grequests"
	"net/http"
)

var (
	Options = &grequests.RequestOptions{}
	Request = grequests.NewSession(Options)
)

func init() {
	Options.UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 " +
		"(KHTML, like Gecko) Chrome/70.0.3626.109 Safari/530.37"
	Options.Headers = map[string]string{}
}

func Get(url string, ro *grequests.RequestOptions) (response *grequests.Response) {
	response, err := Request.Get(url, ro)

	if err != nil {
		panic(err)
	}

	return
}

func SimpleGet(url string) (response *grequests.Response) {
	return Get(url, Options)
}

func GetWithHeaders(url string, headers *map[string]string) (response *grequests.Response) {
	options := *Options
	options.Headers = *headers

	return Get(url, &options)
}

func GetWithCookies(url string, cookies []*http.Cookie) (response *grequests.Response) {
	options := *Options
	options.Cookies = cookies

	return Get(url, &options)
}

func Post(url string, ro *grequests.RequestOptions) (response *grequests.Response) {
	response, err := Request.Post(url, ro)

	if err != nil {
		panic(err)
	}

	return
}

func SimplePost(url string, json interface{}) (response *grequests.Response) {
	options := *Options
	options.Headers["Content-Type"] = "application/json"
	options.JSON = json

	return Post(url, &options)
}

func PostWithJsonAndCookies(url string, cookies []*http.Cookie, json interface{}) (response *grequests.Response) {
	options := *Options
	options.Headers["Content-Type"] = "application/json"
	options.Cookies = cookies
	options.JSON = json

	return Post(url, &options)
}
