package MyRequest

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/levigross/grequests"
	"net/http"
	"net/url"
	"testing"
)

type httpBinComResponse struct {
	Args    map[string]interface{} `json:"args"`
	Headers map[string]interface{} `json:"headers"`
	Origin  string                 `json:"origin"`
	URL     string                 `json:"url"`
	JSON    map[string]interface{} `json:"json"`
}

func TestSimpleGet(t *testing.T) {
	var resp *httpBinComResponse
	_ = SimpleGet("https://httpbin.org/get").JSON(&resp)
	spew.Dump(resp)
}

func TestGetWithHeaders(t *testing.T) {
	var resp *httpBinComResponse
	_ = GetWithHeaders("https://httpbin.org/get", &map[string]string{
		"Accept": "application/json",
	}).JSON(&resp)
	spew.Dump(resp)
}

func TestGetWithCookies(t *testing.T) {
	var resp *httpBinComResponse
	_ = GetWithCookies("https://httpbin.org/get", []*http.Cookie{
		{
			Name:  "name",
			Value: "value",
		},
	}).JSON(&resp)
	spew.Dump(resp)
}

func TestSimplePost(t *testing.T) {
	type postJSON struct {
		A int `json:"a"`
	}
	var resp httpBinComResponse
	_ = SimplePost("https://httpbin.org/post", postJSON{
		A: 1,
	}).JSON(&resp)
	//_ = SimplePost("https://httpbin.org/post", map[string]int{
	//	"A": 1,
	//}).JSON(&resp)
	spew.Dump(resp)
}

func TestPostWithJsonAndCookies(t *testing.T) {
	var resp httpBinComResponse
	_ = PostWithJsonAndCookies(
		"https://httpbin.org/post",
		[]*http.Cookie{
			{
				Name:  "name",
				Value: "value",
			},
		},
		map[string]int{
			"A": 1,
		},
	).JSON(&resp)
	spew.Dump(resp)
}

func TestProxy(t *testing.T) {
	options := &grequests.RequestOptions{}
	options.Data = map[string]string{
		"a": "1",
	}
	options.JSON = options.Data
	if proxy, err := url.Parse("http://127.0.0.1:1087"); err != nil {
		panic("")
	} else {
		options.Proxies = map[string]*url.URL{
			proxy.Scheme: proxy,
			//"https": proxy,
		}
	}

	resp := Post("https://httpbin.org/post", options)
	//resp := Post("https://www.google.com/", options)
	fmt.Println(resp.String())
}

func TestBasicCopy(t *testing.T) {
	// Deep Copy struct
	type Foo struct {
		I int
	}

	var x = &Foo{I: 3}
	var y = *x
	var z = x
	var s = *x

	x.I = 5

	fmt.Printf("x.I = %d, y.I = %d, z.I = %d, s.I = %d\n", x.I, y.I, z.I, s.I)
}
